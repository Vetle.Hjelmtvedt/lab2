package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    
    // Maximum capacity of the fridge
    private int maxCapacity;

    // List of items in the fridge
    private List<FridgeItem> items;

    public Fridge() {
        this.maxCapacity = 20;
        this.items = new ArrayList<>();
    }

    @Override
    public int nItemsInFridge() {
        return items.size();
    }

    @Override
    public int totalSize() {
        return maxCapacity;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (items.size() < totalSize()) {
            items.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        // Check if item in fridge
        if (items.contains(item)) {
            items.remove(item);
        } else {
            throw new NoSuchElementException("This item could not be found in the fridge");
        }
        
    }

    @Override
    public void emptyFridge() {
        items.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredItems = new ArrayList<>();

        for (FridgeItem item : items) {
            if (item.hasExpired()) {
                expiredItems.add(item);
            }
        }

        // Remove expired items
        items.removeAll(expiredItems);
        
        return expiredItems;
    }
}
